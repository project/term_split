CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Term split allows users to split taxonomy terms in two and to divide any
referencing content between the new terms.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/term_split
 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/term_split


REQUIREMENTS
------------

This module requires the following module:

 * Hook Event Dispatcher - https://www.drupal.org/project/hook_event_dispatcher
 * Term Reference Change - https://www.drupal.org/project/term_reference_change

INSTALLATION
------------

Install the module as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information. Note that
there are two dependencies.


CONFIGURATION
-------------

 * This module requires no configuration.


MAINTAINERS
-----------

Current maintainers:

 * Chris Jansen
   Drupal: https://www.drupal.org/u/legolasbo
