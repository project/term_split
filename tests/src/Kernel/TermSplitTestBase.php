<?php

namespace Drupal\Tests\term_split\Kernel;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;

/**
 * Provides a base class for Term Split kernel tests.
 */
abstract class TermSplitTestBase extends KernelTestBase {

  use TaxonomyTestTrait;
  use NodeCreationTrait;
  use ContentTypeCreationTrait;
  use EntityReferenceTestTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'filter',
    'field',
    'node',
    'taxonomy',
    'term_reference_change',
    'term_split',
    'text',
    'user',
    'system',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The vocabulary.
   *
   * @var \Drupal\taxonomy\Entity\Vocabulary
   */
  protected $vocabulary;

  /**
   * The term storage.
   *
   * @var \Drupal\taxonomy\TermStorage
   */
  protected $termStorage;

  /**
   * The private temp store factory.
   *
   * @var \Drupal\user\PrivateTempStoreFactory
   */
  protected $privateTempStore;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installConfig(['filter']);
    $this->installSchema('system', ['key_value_expire', 'sequences']);
    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('taxonomy_term');
    $this->installEntitySchema('taxonomy_vocabulary');
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installConfig('node');
    $this->setUpContentType();

    $this->setUpPrivateTempStore();

    $this->entityTypeManager = \Drupal::entityTypeManager();
    $this->vocabulary = $this->createVocabulary();
    $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
  }

  /**
   * Sets up the content type.
   */
  protected function setUpContentType() {
    $bundle = 'page';
    $this->createContentType([
      'type' => $bundle,
      'name' => 'Basic page',
      'display_submitted' => FALSE,
    ]);

    $entityType = 'node';
    $fieldName = 'field_terms';
    $fieldLabel = 'Terms';
    $targetEntityType = 'taxonomy_term';
    $this->createEntityReferenceField($entityType, $bundle, $fieldName, $fieldLabel, $targetEntityType);
  }

  /**
   * Sets up the private temp store.
   *
   * @throws \Exception
   */
  protected function setUpPrivateTempStore() {
    $account = self::getMock(AccountInterface::class);
    $account->method('id')->willReturn(24);
    $this->container->get('current_user')->setAccount($account);
    $this->privateTempStore = $this->container->get('user.private_tempstore');
  }

}
